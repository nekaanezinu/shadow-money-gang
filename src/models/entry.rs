use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Entry {
    pub id: i32,
    pub date: DateTime<Utc>,
    pub price: i64,
    pub ticker_id: i64,
}
