use chrono::Utc;
use sqlx::{Pool, Postgres};

use crate::{
    models::{entry::Entry, ticker::Ticker},
    repositories::{entries, tickers},
};

pub async fn insert_entry(pool: Pool<Postgres>) -> Result<Entry, sqlx::Error> {
    let repo = entries::EntryRepo::new(pool);
    let attrs = entries::EntryAttributes {
        id: None,
        date: Utc::now(),
        price: 200,
        ticker_id: 1,
    };
    match repo.create(attrs).await {
        Ok(result) => match result {
            Some(record) => Ok(record),
            None => Err(sqlx::Error::RowNotFound),
        },
        Err(e) => Err(e),
    }
}

pub async fn insert_ticker(pool: Pool<Postgres>) -> Result<Ticker, sqlx::Error> {
    let repo = tickers::TickerRepo::new(pool);
    let attrs = tickers::TickerAttributes {
        id: None,
        name: "Apple Inc.".to_string(),
    };
    match repo.create(attrs).await {
        Ok(result) => match result {
            Some(record) => Ok(record),
            None => Err(sqlx::Error::RowNotFound),
        },
        Err(e) => Err(e),
    }
}
