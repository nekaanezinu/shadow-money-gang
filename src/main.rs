use rocket::http::Status;
use rocket::response::{self, Responder, Response};
use rocket::serde::json::Json;
use rocket::Request;
use rocket::State;
use serde::Serialize;
use shuttle_secrets::SecretStore;
use sqlx::PgPool;
use std::io::Cursor;

use crate::models::ticker::Ticker;

#[macro_use]
extern crate rocket;

pub mod models;
pub mod playground;
pub mod repositories;

#[derive(Serialize)]
struct ApiError {
    message: String,
}

struct JsonError(Json<ApiError>);

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[get("/db_pool_test")]
async fn db_pool_test(db_pool: &State<PgPool>) -> Result<Json<Ticker>, JsonError> {
    match playground::repo_test::insert_ticker(db_pool.inner().to_owned()).await {
        Ok(ticker) => return Ok(Json(ticker)),
        Err(e) => {
            return Err(JsonError(Json(ApiError {
                message: e.to_string(),
            })))
        }
    }
}

#[shuttle_runtime::main]
async fn rocket(
    #[shuttle_shared_db::Postgres(
        local_uri = "postgres://{secrets.pg_user}:{secrets.pg_password}@{secrets.pg_url}:{secrets.pg_port}/postgres"
    )]
    pool: PgPool,
    #[shuttle_secrets::Secrets] _secret_store: SecretStore,
) -> shuttle_rocket::ShuttleRocket {
    sqlx::migrate!()
        .run(&pool)
        .await
        .expect("migrations failed wtf");

    let rocket = rocket::build()
        .manage(pool)
        .mount("/", routes![index, db_pool_test]);

    Ok(rocket.into())
}

#[rocket::async_trait]
impl<'r> Responder<'r, 'static> for JsonError {
    fn respond_to(self, _: &'r Request<'_>) -> response::Result<'static> {
        Response::build()
            .sized_body(self.0.message.len(), Cursor::new(self.0.message.clone()))
            .header(rocket::http::ContentType::JSON)
            .status(Status::BadRequest)
            .ok()
    }
}
