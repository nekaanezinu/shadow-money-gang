use chrono::{DateTime, Utc};
use sqlx::Row;

use crate::models::entry::Entry;

pub struct EntryAttributes {
    pub id: Option<i32>,
    pub date: DateTime<Utc>,
    pub price: i64,
    pub ticker_id: i64,
}

pub struct EntryRepo {
    pool: sqlx::PgPool,
}

impl EntryRepo {
    pub fn new(pool: sqlx::PgPool) -> Self {
        EntryRepo { pool }
    }

    pub async fn find(&self, id: i32) -> Result<Option<Entry>, sqlx::Error> {
        sqlx::query_as!(Entry, "SELECT * FROM entries WHERE id = $1", id)
            .fetch_optional(&self.pool)
            .await
    }

    pub async fn create(&self, attributes: EntryAttributes) -> Result<Option<Entry>, sqlx::Error> {
        let row = sqlx::query_as!(
            Entry,
            "INSERT INTO entries (date, price, ticker_id) VALUES ($1, $2, $3)",
            attributes.date,
            attributes.price,
            attributes.ticker_id
        )
        .fetch_one(&self.pool)
        .await;

        match row {
            Ok(result) => self.find(result.get("id")).await,
            Err(e) => Err(e),
        }
    }
}
