use sqlx::Row;

use crate::models::ticker::Ticker;

pub struct TickerAttributes {
    pub id: Option<i32>,
    pub name: String,
}

pub struct TickerRepo {
    pool: sqlx::PgPool,
}

impl TickerRepo {
    pub fn new(pool: sqlx::PgPool) -> Self {
        Self { pool }
    }

    pub async fn find(&self, id: i32) -> Result<Option<Ticker>, sqlx::Error> {
        sqlx::query_as!(Ticker, "SELECT * FROM tickers WHERE id = $1", id)
            .fetch_optional(&self.pool)
            .await
    }

    pub async fn create(
        &self,
        attributes: TickerAttributes,
    ) -> Result<Option<Ticker>, sqlx::Error> {
        let row = sqlx::query_as!(
            Ticker,
            "INSERT INTO tickers (name) VALUES ($1)",
            attributes.name,
        )
        .fetch_one(&self.pool)
        .await;
        //FIXME: for some reason this gets executed twice and the second one fails
        // with a duplicate key error. cba to fix it now.

        match row {
            Ok(result) => self.find(result.get("id")).await,
            Err(e) => Err(e),
        }
    }
}
