-- Add migration script here
CREATE TABLE IF NOT EXISTS entries (
  id serial PRIMARY KEY,
  date timestamptz NOT NULL,
  price bigint NOT NULL,
  ticker_id bigint NOT NULL
);

CREATE INDEX IF NOT EXISTS entries_ticker_id ON entries (ticker_id);