-- Add migration script here
CREATE TABLE IF NOT EXISTS tickers (
  id serial PRIMARY KEY,
  name VARCHAR(100) UNIQUE NOT NULL
);